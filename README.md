# MelodySmartKit Sample App

This sample app demonstrates how to integrate and use the MelodySmartKit framework for communicating with BlueCreation's MelodySmart product line.

## Installation

1. Clone
2. Install [Carthage](https://github.com/Carthage/Carthage)
3. Run `carthage update`
4. Drag the built .framework in the project
5. Run the app

