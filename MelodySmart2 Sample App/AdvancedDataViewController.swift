//
//  MelodyAdvancedViewController.swift
//  Melody
//
//  Created by Stanislav Nikolov on 27/09/2016.
//
//

import UIKit
import MessageUI
import MelodySmartKit

class AdvancedDataViewController: UIViewController, MFMailComposeViewControllerDelegate, MelodySmartDeviceListener, MelodySmartDeviceViewController {

    @IBOutlet weak var swEcho: UISwitch!
    @IBOutlet weak var swLogging: UISwitch!
    @IBOutlet weak var lblFileSize: UILabel!
    @IBOutlet weak var tfMtuSize: UITextField!

    var device: MelodySmartDevice?
    var echoEnabled: Bool { return swEcho.isOn }
    var loggingEnabled: Bool { return swLogging.isOn }
    var chunkSize: Int { return Int(tfMtuSize.text!, radix: 10) ?? 40 }
    var loggedData = Data()


    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        device?.addListener(self)
    }

    override func viewDidDisappear(_ animated: Bool) {
        device?.removeListener(self)

        super.viewWillDisappear(animated)
    }

    func melodySmartDidReceiveData(_ data: [UInt8], fromDevice device: MelodySmartDevice) {
        let dataAsData = Data(bytes: data)

        if echoEnabled {
            if !device.sendData(dataAsData) {
                showDataSendingError()
            }
        }

        if loggingEnabled {
            loggedData.append(dataAsData)
            updateFileSize()
        }
    }

    func showDataSendingError() {
        DispatchQueue.main.sync {
            UIAlertView(title: "Error", message: "Failed to send data packet", delegate: nil, cancelButtonTitle: "OK").show()
        }
    }

    func updateFileSize() {
        lblFileSize.text = "Logged Data Size: \(loggedData.count)b"
    }

    @IBAction func btnClearFIle_TouchUpInside(_ sender: AnyObject) {
        loggedData.removeAll()
        updateFileSize()
    }

    @IBAction func btnSendIap_TouchUpInside(_ sender: AnyObject) {
        DispatchQueue.global().async {
            while self.loggedData.count > 0 {
                let tx = min(self.loggedData.count, self.chunkSize)
                let data = Data(self.loggedData[0..<tx])

                if !self.device!.sendData(data) {
                    self.showDataSendingError()
                }

                self.loggedData = Data(self.loggedData.dropFirst(tx))

                DispatchQueue.main.async {
                    self.updateFileSize()
                }
            }
        }
    }

    @IBAction func btnSendEmail_TouchUpInside(_ sender: AnyObject) {
        guard MFMailComposeViewController.canSendMail() else {
            UIAlertView(title: "Couldn't send mail",
                        message: "You iDevice is not currently configured for sending email.",
                        delegate: nil,
                        cancelButtonTitle: "OK")
                .show()
            return
        }

        guard loggedData.count > 0 else {
            UIAlertView(title: "Couldn't send mail",
                        message: "No Data Saved",
                        delegate: nil,
                        cancelButtonTitle: "OK")
                .show()
            return
        }

        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject("Your Melody saved data.")
        mailComposer.setMessageBody("Here's the data you've received", isHTML: false)
        mailComposer.addAttachmentData(loggedData, mimeType: "application/octet-stream", fileName: "melody_data.bin")

        self.present(mailComposer, animated: true, completion: nil)
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
}
