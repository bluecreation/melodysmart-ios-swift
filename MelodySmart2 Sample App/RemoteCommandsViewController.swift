//
//  MSRemoteCommandsViewController.swift
//  MelodySmart_Example
//
//  Created by Stanislav Nikolov on 15/01/2015.
//  Copyright (c) 2015 Blue Creation. All rights reserved.
//

import UIKit
import MelodySmartKit

class RemoteCommandsViewController: UIViewController, UITextFieldDelegate, MelodySmartDeviceListener, MelodySmartDeviceViewController {
    @IBOutlet var tfCommand: UITextField!
    @IBOutlet var tvOutput: UITextView!

    internal var device: MelodySmartDevice?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        device?.addListener(self)
    }

    override func viewDidDisappear(_ animated: Bool) {
        device?.removeListener(self)

        super.viewWillDisappear(animated)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard device!.sendCommand(tfCommand.text!) else {
            return false
        }

        textField.resignFirstResponder()

        return true
    }

    func melodySmartDidConnectDevice(_ device: MelodySmartDevice, inBootMode bootMode: (MelodySmartDevice.BootMode)) {
    }
    
    func melodySmartDidConnectDevice(_ device: MelodySmartDevice) {
    }

    func melodySmartDidDisconnectDevice(_ device: MelodySmartDevice) {
    }

    func melodySmartDidReceiveData(_ data: [UInt8], fromDevice device: MelodySmartDevice) {
    }

    func melodySmartDidReceiveCommandOutput(_ output: String, fromDevice device: MelodySmartDevice) {
        tvOutput.text? += output + "\n"

        tvOutput.scrollRangeToVisible(NSRange(location: tvOutput.text.characters.count, length: 1))
    }

    func melodySmartDidReceiveI2cData(_ data: [UInt8], fromDevice device: MelodySmartDevice, success: Bool) {
    }
}
