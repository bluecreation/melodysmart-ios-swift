//
//  DetailViewController.swift
//  MelodySmart2 Sample App
//
//  Created by Stanislav Nikolov on 04/05/2016.
//  Copyright © 2016 BlueCreation. All rights reserved.
//

import UIKit
import MelodySmartKit

protocol MelodySmartDeviceViewController {
    var device: MelodySmartDevice? { get set }
}

class DetailViewController: UIViewController, MelodySmartDeviceListener, MelodySmartDeviceViewController {

    @IBOutlet weak var tfOutgoingData: UITextField!
    @IBOutlet weak var tfIncomingData: UITextField!

    @IBOutlet weak var btnRemoteCommands: UIButton!
    @IBOutlet weak var btnI2cControl: UIButton!
    @IBOutlet weak var btnOTAU: UIButton!
    @IBOutlet weak var btnAdvancedData: UIButton!
    @IBOutlet weak var btnDisconnect: UIButton!

    var device: MelodySmartDevice? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        updateConnectiongStatus()
    }

    @IBAction func btnDisconnect_TouchUpInside(_ sender: AnyObject) {
        device?.disconnect()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        device?.addListener(self)

        if (device!.state == .disconnected) {
            connectDevice()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        device?.removeListener(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func enableButtons(_ enable: Bool) {
        btnI2cControl?.isEnabled = enable
        btnRemoteCommands?.isEnabled = enable
        btnOTAU?.isEnabled = enable
        btnAdvancedData?.isEnabled = enable
        btnDisconnect?.isEnabled = enable
    }

    func updateConnectiongStatus() {
        let buttonEnabled: Bool
        let status: String

        switch device!.state {
        case .connected:
            status = "Connected"
            buttonEnabled = true

        case .connecting:
            status = "Connecting..."
            buttonEnabled = false

        case .disconnecting:
            status = "Disconnecting..."
            buttonEnabled = false

        case .disconnected:
            status = "Disconnected"
            buttonEnabled = false
        }

        enableButtons(buttonEnabled)
        title = "\(device!.name ?? "<Unknown>") (\(status))"
    }

    func connectDevice() {
        device!.connect()
        updateConnectiongStatus()
    }
    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var controller = segue.destination as! MelodySmartDeviceViewController
        controller.device = device
    }

    // MARK: - MelodySmartDevice listener

    func melodySmartDidConnectDevice(_ device: MelodySmartDevice, inBootMode bootMode: (MelodySmartDevice.BootMode)) {
        updateConnectiongStatus()
    }

    func melodySmartDidDisconnectDevice(_ device: MelodySmartDevice) {
        updateConnectiongStatus()
    }

    func melodySmartDidReceiveData(_ data: [UInt8], fromDevice device: MelodySmartDevice) {
        let stringData = String(bytes: data, encoding: String.Encoding.utf8)
        tfIncomingData.text = stringData
    }

    func melodySmartDidReceiveCommandOutput(_ output: String, fromDevice device: MelodySmartDevice) {
    }

    func melodySmartDidReceiveI2cData(_ data: [UInt8], fromDevice device: MelodySmartDevice, success: Bool) {
    }

    // MARK: - UITextField delegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard textField === self.tfOutgoingData else {
            return false
        }

        if !device!.sendString(textField.text!) {
            print("Send error")
        }

        textField.resignFirstResponder()

        return true
    }
}

