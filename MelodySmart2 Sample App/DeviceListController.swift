//
//  MasterViewController.swift
//  MelodySmart2 Sample App
//
//  Created by Stanislav Nikolov on 04/05/2016.
//  Copyright © 2016 BlueCreation. All rights reserved.
//

import UIKit
import MelodySmartKit

class MasterViewController: UITableViewController, MelodySmartManagerListener {

    struct DiscoveredDevice {
        var device: MelodySmartDevice
        var RSSI: NSNumber
    }

    var detailViewController: DetailViewController? = nil
    var objects = [DiscoveredDevice]()

    var melodyManager: MelodySmartManager?

    override func viewDidLoad() {
        super.viewDidLoad()

        melodyManager = MelodySmartManager()

        let addButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(startScan(_:)))

        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        melodyManager!.addListener(self)

        if (melodyManager!.state == .poweredOn) {
            melodyManager!.scan()
        }

        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed

        self.tableView.reloadData()
    }

    override func viewWillDisappear(_ animated: Bool) {
        print("viewWillDisappear")

        melodyManager!.stopScan()
        melodyManager!.removeListener(self)

        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func startScan(_ sender: AnyObject) {
        print("starting scanning")

        let indexPaths = (0..<objects.count).map({ IndexPath(row: $0, section: 0) })

        objects.removeAll()

        tableView.deleteRows(at: indexPaths, with: .automatic)

        melodyManager?.scan()
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = objects[(indexPath as NSIndexPath).row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.device = object.device
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object = objects[(indexPath as NSIndexPath).row]
        cell.textLabel!.text = (object.device.name ?? "<Unkown>") + ((object.device.state == .connected) ? " (Connected)" : "")
        cell.detailTextLabel!.text = "\(object.RSSI)"
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            objects.remove(at: (indexPath as NSIndexPath).row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }

    // MelodySmartManagerListener

    func melodySmartManager(_ manager: MelodySmartManager, didUpdateState state: MelodySmartManager.State) {
        print("new state \(state)")

        if (state == .poweredOn) {
        }
    }
    
    func melodySmartManager(_ manager: MelodySmartManager, didConnectDevice device: MelodySmartDevice) {
    }
    
    func melodySmartManager(_ manager: MelodySmartManager, didDisconnectDevice device: MelodySmartDevice) {
    }
    
    func melodySmartManager(_ manager: MelodySmartManager, didDiscoverDevice device: MelodySmartDevice, advertisementData: MelodySmartManager.AdvertisementData) {
        let rssi = NSNumber(value: advertisementData.RSSI)

        if let knownDeviceIndex = objects.index(where: { $0.device === device }) {
            objects[knownDeviceIndex].RSSI = rssi
            tableView.reloadRows(at: [ IndexPath(row: knownDeviceIndex , section: 0) ], with: .none)
        } else {
            let newDevice = DiscoveredDevice(device: device, RSSI: rssi)
            objects.append(newDevice)
            tableView.insertRows(at: [ IndexPath(row: objects.count - 1, section: 0) ], with: .automatic)
        }
    }
}

