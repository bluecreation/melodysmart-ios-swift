//
//  MSOtauViewController.swift
//  MelodySmart_Example
//
//  Created by Stanislav Nikolov on 16/01/2015.
//  Copyright (c) 2015 Blue Creation. All rights reserved.
//

import UIKit
import MelodySmartKit

class MelodyRelease {
    var version: String!
    var imageFileUrl: URL!
    var keyFileUrl: URL!
    var releaseDate: Date!
}

class OtauViewController: UIViewController, MelodySmartDeviceListener, MelodySmartDeviceViewController {

    @IBOutlet var lblImage: UILabel!
    @IBOutlet var lblDeviceVersion: UILabel!
    @IBOutlet var lblProgress: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var btnStartOtau: UIButton!
    @IBOutlet var pvProgress: UIProgressView!

    fileprivate var imageData: Data?
    fileprivate var keyData: Data?

    var device: MelodySmartDevice?

    func melodySmartDidConnectDevice(_ device: MelodySmartDevice, inBootMode bootMode: (MelodySmartDevice.BootMode)) {
        switch bootMode {
        case .application:
            updateUi()

        case .bootloader:
            btnStartOtau.isEnabled = false
            guard let id = imageData, let kfd = keyData else {
                print("No Image data or key file data found!")
                return
            }

            device.startOtauWithImageData(id, keyFileData: kfd)

        default:
            break
        }
    }

    func melodySmartDidUpdateOtauState(_ otauState: MelodySmartDevice.OtauState, forDevice device: MelodySmartDevice) {
        switch otauState {
        case .idle:
            lblStatus.text = "Idle"

        case .starting:
            lblStatus.text = "Starting..."

        case .inProgress(let progress):
            lblStatus.text = "Update In Progress..."
            lblProgress.text = "\(progress)%"
            pvProgress.progress = Float(progress) / 100.0

        case .complete:
            lblStatus.text = "Update successful!"

        case .failed:
            lblStatus.text = "Update failed!"
        }
    }

    @IBAction func btnStartOtau_onTouchedUpInside() {
        lblStatus.text = "Downloading FW image..."

        DispatchQueue.global(qos: DispatchQoS.default.qosClass).async {
            self.imageData = try? Data(contentsOf: self.melodyRelease!.imageFileUrl)
            self.keyData = try? Data(contentsOf: self.melodyRelease!.keyFileUrl)

            guard self.imageData != nil && self.keyData != nil else {
                self.lblStatus.text = "Error"
                UIAlertView(title: "Error", message: "Error downloading FW image, please try again!", delegate: nil, cancelButtonTitle: "OK").show()
                return
            }

            let rebootOk = self.device!.rebootToOtauMode()
            
            DispatchQueue.main.async(execute: {
                self.lblStatus.text = rebootOk ? "Rebooting device" : "Failed to reboot"
            })
        }
    }

    var melodyRelease: MelodyRelease? {
        didSet { updateUi() }
    }

    func updateUi() {
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.short

        if let mr = melodyRelease {
            lblImage.text = "Selected FW: \(mr.version!) @ \(formatter.string(from: mr.releaseDate))"
        }
        btnStartOtau.isEnabled = melodyRelease != nil
        lblDeviceVersion.text = "Device FW: \(device!.firmwareVersion ?? "Unknown")"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        updateUi()
    }

    override func viewWillAppear(_ animated: Bool) {
        device?.addListener(self)
    }

    override func viewWillDisappear(_ animated: Bool) {
        device?.removeListener(self)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "image_selection" {
            (segue.destination as! OtauImageSelectionTableViewController).delegate = self;
        }
    }

}
