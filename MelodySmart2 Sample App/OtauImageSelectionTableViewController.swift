//
//  MSOtauImageSelectionTableViewController.swift
//  MelodySmart_Example
//
//  Created by Stanislav Nikolov on 16/01/2015.
//  Copyright (c) 2015 Blue Creation. All rights reserved.
//

import UIKit

class OtauImageSelectionTableViewController: UITableViewController {

    var delegate: OtauViewController!

    fileprivate let BASE_URL = URL(string: "https://bluecreation.com/stan/melody_smart_otau/")!

    fileprivate var availableImages = [MelodyRelease]()

    fileprivate var loadingDialog: UIAlertView?

    override func viewDidLoad() {
        super.viewDidLoad()

        loadingDialog = UIAlertView(title: "Loading FW versions", message: "Please wait...", delegate: nil, cancelButtonTitle: nil)
        loadingDialog?.show()

        DispatchQueue.global(qos: DispatchQoS.default.qosClass).async {
            guard let manifestData = try? Data(contentsOf: self.BASE_URL.appendingPathComponent("manifest.json")) else {
                self.loadingDialog?.dismiss(withClickedButtonIndex: 0, animated: true)
                return;
            }

            let jsonObject = try? JSONSerialization.jsonObject(with: manifestData, options: []) 

            guard let rootObject = jsonObject as? [String: Any] else {
                self.loadingDialog?.dismiss(withClickedButtonIndex: 0, animated: true)
                return;
            }

            guard let defaultKeyfile = rootObject["default_keyfile"] as? String else {
                self.loadingDialog?.dismiss(withClickedButtonIndex: 0, animated: true)
                return;
            }
            
            guard let releases = rootObject["releases"] as? [[String: Any]] else {
                self.loadingDialog?.dismiss(withClickedButtonIndex: 0, animated: true)
                return;
            }

            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"

            for release in releases {
                let melodyRelease = MelodyRelease()
                melodyRelease.version = release["version"] as! String
                melodyRelease.imageFileUrl = self.BASE_URL.appendingPathComponent(release["file_name"] as! String)

                let keyfile = release["keyfile"] as! String?
                melodyRelease.keyFileUrl = self.BASE_URL.appendingPathComponent(keyfile ?? defaultKeyfile)
                melodyRelease.releaseDate = formatter.date(from: release["release_date"] as! String)!

                self.availableImages.append(melodyRelease)
            }

            DispatchQueue.main.async(execute: {
                self.loadingDialog?.dismiss(withClickedButtonIndex: 0, animated: true)
                self.tableView.reloadData()
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return availableImages.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) 

        let formatter = DateFormatter()
        formatter.dateStyle = .medium

        cell.textLabel?.text = availableImages[(indexPath as NSIndexPath).row].version
        cell.detailTextLabel?.text = formatter.string(from: availableImages[(indexPath as NSIndexPath).row].releaseDate as Date)

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.melodyRelease = availableImages[(indexPath as NSIndexPath).row]
        _ = navigationController?.popViewController(animated: true)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
}
